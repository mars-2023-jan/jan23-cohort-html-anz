document.getElementById("startover_div").style.display = "none"
var count = 1
var inputValue

function check(){
    inputValue = document.getElementById("inputNumber").value
    if(inputValue!="")
    {
        if((inputValue < 1) || (inputValue > 10)){ 
            if(count == 4){ 
                document.getElementById("message").innerHTML = "You have reached more than 3 attempts.";
                document.getElementById("inputNumber").value = ""
                document.getElementById("startover_div").style.display = "block"
                document.getElementById("checkButton").disabled = true;
            }else{
                document.getElementById("message").innerHTML = "Enter a number between 1 and 10.";
                document.getElementById("inputNumber").value = ""
                count = count + 1
            }

        }else{
        document.getElementById("inputNumber").value = ""
        if(count == 4){ 
            document.getElementById("message").innerHTML = "You have reached more than 3 attempts";
            document.getElementById("inputNumber").value = ""
            document.getElementById("startover_div").style.display = "block"
            document.getElementById("checkButton").disabled = true;
        }else{
        const myLuckyNum = 6
            if(inputValue!=myLuckyNum){
                document.getElementById("message").innerHTML = "You got it wrong!!!";
                count = count + 1
            }
            else{
                if(count == 1){
                document.getElementById("message").innerHTML = "You guessed right in 1 try!";
                count = 1
                }else{
                    document.getElementById("message").innerHTML = "You guessed right in "+count+" tries!";
                    count = 1
                }
            }
        }
    }
        

    }else{
        document.getElementById("message").innerHTML = "Enter a number between 1 and 10.";
        document.getElementById("inputNumber").value = ""
    }
}
function clearInput(){
    document.getElementById("inputNumber").value = ""
    document.getElementById("message").innerHTML = ""
}
function startOver(){
    document.getElementById("inputNumber").value = ""
    document.getElementById("message").innerHTML = ""
    document.getElementById("startover_div").style.display = "none"
    document.getElementById("checkButton").disabled = false;
    count = 1
}
