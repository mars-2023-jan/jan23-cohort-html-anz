let dateArray = []
let mortgageArray = []
let gasArray = []
let childcareArray = []
let insuranceArray = []
let paymentTimeArray = []
let miscArray = []

function addDetails(){
    let date = document.getElementById("date_id").value
    let mortgage = document.getElementById("mortgage_id").value
    let gas = document.getElementById("gas_id").value
    let childCare = document.getElementById("child_care_id").value
    let insurance = document.getElementById("insurance_id").value
    let selectedRadio = document.querySelector('input[name="monthlyOryearly"]:checked').value
    let misc = document.getElementById("misc_id").value
    //let utility = document.querySelectorAll('input[id="checkbox1"]:checked').value

    dateArray.push(date)
    mortgageArray.push(mortgage)
    gasArray.push(gas)
    childcareArray.push(childCare)
    insuranceArray.push(insurance)
    paymentTimeArray.push(selectedRadio)
    miscArray.push(misc)

    window.localStorage.setItem('date',JSON.stringify(dateArray))
    window.localStorage.setItem('mortgage',JSON.stringify(mortgageArray))
    window.localStorage.setItem('gas',JSON.stringify(gasArray))
    window.localStorage.setItem('childcare',JSON.stringify(childcareArray))
    window.localStorage.setItem('insurance',JSON.stringify(insuranceArray))
    window.localStorage.setItem('paymentTime',JSON.stringify(paymentTimeArray))
    window.localStorage.setItem('misc',JSON.stringify(miscArray))
}