let employeeArray = []

class Employee{
    #id
    #ename
    #designation
    #hours
    #salary
    constructor(id,ename,designation,hours,salary){
        this.id = id
        this.ename = ename
        this.designation = designation
        this.hours = hours
        this.salary = salary

    }

    set id(id){
        if(id === ''){
            throw new Error('Id is empty')
        }
        this.#id = id
    }
    get id(){
        return this.#id
    }
    set name(ename){
        if(ename === ''){
            throw new Error('Name is empty')
        }
        this.#ename = ename
    }
    get name(){
        return this.#ename
    } 
    set designation(designation){
        if(designation === ''){
            throw new Error('Designation is empty')
        }
        this.#designation = designation
    }
    get designation(){
        return this.#designation
    } 
    set hours(hours){
        if(hours === ''){
            throw new Error('Hours is empty')
        }
        this.#hours = hours
    }
    get hours(){
        return this.#hours
    }
    set salary(salary){
        if(salary === ''){
            throw new Error('Salary is empty')
        }
        this.#salary = salary
    }
    get salary(){
        return this.#salary
    }
    
    show(){
        console.log(this.id)
        console.log(this.ename)
        console.log(this.designation)
        console.log(this.hours)
        console.log(this.salary)
    }
}



function addData(){
    let id = document.getElementById('emp_employee_id').value
    let ename = document.getElementById('emp_name_id').value
    let designation = document.getElementById('emp_designation_id').value
    let hours = document.getElementById('emp_hours_id').value
    let salary = 0  

    if(designation.toLowerCase() == 'manager'){
       
        salary= hours * 50
       
        message = ename + " who is a "+designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else if(designation.toLowerCase() == 'consultant'){

        salary = hours * 30
        message = ename + " who is a "+designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else if(designation.toLowerCase() == 'trainee'){

        salary = hours * 20
        message = ename + " who is a "+designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else{
        salary= 0
        document.getElementById("display").innerHTML = "Error"
   }

    const emp = new Employee(id,ename,designation,hours,salary);
    employeeArray.push(emp)

    emp.show();
    document.getElementById('emp_employee_id').value = ''
    document.getElementById('emp_name_id').value = ''
    document.getElementById('emp_designation_id').value = ''
    document.getElementById('emp_hours_id').value = ''

}

function maxSalary(){

    var max_salary = Math.max.apply(Math,employeeArray.map(function(o){return o.salary;}))
    let obj = employeeArray.find(o => o.salary === max_salary);
    message = obj.ename + " who is a "+obj.designation+", worked "+obj.hours+" hours and gets the highest salary of $"+obj.salary
    document.getElementById("display").innerHTML = message

}