
let employee = []
let message = ''
function addData(){
    let emp_id = document.getElementById('emp_employee_id').value
    let emp_name = document.getElementById('emp_name_id').value
    let emp_designation = document.getElementById('emp_designation_id').value
    let emp_hours = document.getElementById('emp_hours_id').value
    let emp_salary = 0  

    if(emp_designation.toLowerCase() == 'manager'){
       
        emp_salary= emp_hours * 50
       
        message = emp_name + " who is a "+emp_designation+" gets $"+emp_salary
        document.getElementById("display").innerHTML = message

   }else if(emp_designation.toLowerCase() == 'consultant'){

        emp_salary = emp_hours * 30
        message = emp_name + " who is a "+emp_designation+" gets $"+emp_salary
        document.getElementById("display").innerHTML = message

   }else if(emp_designation.toLowerCase() == 'trainee'){

        emp_salary = emp_hours * 20
        message = emp_name + " who is a "+emp_designation+" gets $"+emp_salary
        document.getElementById("display").innerHTML = message

   }else{
        emp_salary= 0
        document.getElementById("display").innerHTML = "Error"
   }

        employee.push({
            id : emp_id ,
            name : emp_name ,
            designation : emp_designation ,
            hours: emp_hours ,
            salary : emp_salary

        })
        console.log(employee)

        document.getElementById('emp_employee_id').value = ''
        document.getElementById('emp_name_id').value = ''
        document.getElementById('emp_designation_id').value = ''
        document.getElementById('emp_hours_id').value = ''
}
function maxSalary(){

    var max_salary = Math.max.apply(Math,employee.map(function(o){return o.salary;}))
    let obj = employee.find(o => o.salary === max_salary);
    message = obj.name + " who is a "+obj.designation+", worked "+obj.hours+" hours and gets the highest salary of $"+obj.salary
    document.getElementById("display").innerHTML = message

}