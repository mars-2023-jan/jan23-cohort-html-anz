let employee = {}
let message = ''
function addData(){
    employee.id = document.getElementById('emp_employee_id').value
    employee.name = document.getElementById('emp_name_id').value
    employee.designation = document.getElementById('emp_designation_id').value
    employee.hours = document.getElementById('emp_hours_id').value

    
   if(employee.designation.toLowerCase() == 'manager'){
       
        var salary = employee.hours * 50
        message = employee.name + " who is a "+employee.designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else if(employee.designation.toLowerCase() == 'consultant'){

        var salary = employee.hours * 30
        message = employee.name + " who is a "+employee.designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else if(employee.designation.toLowerCase() == 'trainee'){

        var salary = employee.hours * 20
        message = employee.name + " who is a "+employee.designation+" gets $"+salary
        document.getElementById("display").innerHTML = message

   }else{
        document.getElementById("display").innerHTML = "Error"
   }
}