let nameArray = []
let emailArray = []

function dataEnter(){
    let name = document.getElementById("name_id").value
    let email = document.getElementById("email_id").value

    nameArray.push(name)
    emailArray.push(email)

    let table = '<table border=1>'
    table+= '<tr><th>NAME</th><th>EMAIL</th></tr>'

    for( let i=0; i<nameArray.length; i++){
        table+= '<tr>'
        table+=`<td>${nameArray[i]}</td>`
        table+=`<td>${emailArray[i]}</td>`
        table+='</tr>'
    }
    table+= '</table>'
    document.getElementById("display").innerHTML = table
    document.getElementById("name_id").value = ''
    document.getElementById("email_id").value = ''
}