
let employeeArray = []
var message

class Employee{
    #id
    #ename
    #designation
    #hours
    #salary
    constructor(id,ename,designation,hours,salary){
        this.id = id
        this.ename = ename
        this.designation = designation
        this.hours = hours
        this.salary = salary

    }

    set id(id){
        if(id === ''){
            throw new Error('Id is empty')
        }
        this.#id = id
    }
    get id(){
        return this.#id
    }
    set name(ename){
        if(ename === ''){
            throw new Error('Name is empty')
        }
        this.#ename = ename
    }
    get name(){
        return this.#ename
    } 
    set designation(designation){
        if(designation === ''){
            throw new Error('Designation is empty')
        }
        this.#designation = designation
    }
    get designation(){
        return this.#designation
    } 
    set hours(hours){
        if(hours === ''){
            throw new Error('Hours is empty')
        }
        this.#hours = hours
    }
    get hours(){
        return this.#hours
    }
    set salary(salary){
        if(salary === ''){
            throw new Error('Salary is empty')
        }
        this.#salary = salary
    }
    get salary(){
        return this.#salary
    }
    
}


$(function() { //shorthand document.ready function
    $('#form_id').on('submit', function(e) { 
        e.preventDefault();  //prevent form from submitting
        let id = $("#emp_employee_id").val()
        let ename = $("#emp_name_id").val()
        let designation = $("#emp_designation_id").val()
        let hours = $("#emp_hours_id").val()
        let salary = 0 
        console.log(id); 
        if(designation.toLowerCase() == 'manager'){
       
                salary= hours * 50 
                message = ename + " who is a "+designation+" gets $"+salary
                $("#display").text(message)

        }else if(designation.toLowerCase() == 'consultant'){

                salary = hours * 30
                message = ename + " who is a "+designation+" gets $"+salary
                $("#display").text(message)

        }else if(designation.toLowerCase() == 'trainee'){

                salary = hours * 20
                message = ename + " who is a "+designation+" gets $"+salary
                $("#display").text(message)

        }else{
                salary= 0
                $("#display").text("Error")
        }

            const emp = new Employee(id,ename,designation,hours,salary);
            employeeArray.push(emp)

            $("#emp_employee_id").val('')
            $("#emp_name_id").val('')
            $("#emp_designation_id").val('')
            $("#emp_hours_id").val('')
            });
});

$("#max_salary_id").on("click",function(){
    const max_salary = employeeArray.reduce(
        (prev, current) => {
          return prev.salary > current.salary ? prev : current
        }
      );
      console.log(max_salary)
        message = max_salary.ename + " who is a "+max_salary.designation+", worked "+max_salary.hours+" hours and gets the highest salary of $"+max_salary.salary
        console.log(message)
        $("#display").text(message)
})

